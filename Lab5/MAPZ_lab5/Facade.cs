﻿using MAPZ_lab5.Patterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MAPZ_lab5.UIManager;

namespace MAPZ_lab5.Patterns
{
    public class Facade
    {
        private GameModel _model;
        private UIManager _uiManager;
        private ICoinsManager _coinsManager;

        public Facade(MainWindow mainWindow)
        {
            _uiManager = new UIManager(mainWindow);
            _model = new GameModel();
            _coinsManager = new Proxy();
            _coinsManager.AddCoins(10);
        }
        public void InitializeComponents()
        {
            _model.CreateLevel(GetGridForLevel(1));
            _uiManager.DrawGrid(_model.levelsList[0].LevelGrid);

            SetLevelTheme(UIManager.WindowTheme.LabTheme);
        }
        public void SwitchThemes()
        {
            switch (_model.WindowTheme)
            {
                case UIManager.WindowTheme.LabTheme:
                    _model.WindowTheme = UIManager.WindowTheme.SpaceTheme;
                    break;
                case UIManager.WindowTheme.SpaceTheme:
                    _model.WindowTheme = UIManager.WindowTheme.LabTheme;
                    break;
            }

            DefineAndDrawBackground();
            DefineAndDrawSprite(0, 0);
        }
        public void ChangeLevels()
        {
            int levelId = (_model.CurrentLevelId == 1) ? 2 : 1;
            if (levelId > _model.levelsList.Count - 1)
            {
                _model.CreateLevel(GetGridForLevel(levelId));
            }

            _uiManager.DrawGrid(_model.levelsList[levelId - 1].LevelGrid);
            DefineAndDrawSprite(0, 0);
            _model.CurrentLevelId = levelId;
        }
        public void SetLevelTheme(WindowTheme theme)
        {
            switch (theme)
            {
                case UIManager.WindowTheme.LabTheme:
                    _model.WindowTheme = UIManager.WindowTheme.LabTheme;
                    break;
                case UIManager.WindowTheme.SpaceTheme:
                    _model.WindowTheme = UIManager.WindowTheme.SpaceTheme;
                    break;
            }

            DefineAndDrawBackground();
            DefineAndDrawSprite(0, 0);
        }
        private void DefineAndDrawSprite(int xCoord, int yCoord)
        {
            IThemeFactory factory;
            switch (_model.WindowTheme)
            {
                case UIManager.WindowTheme.LabTheme:
                    factory = new LabThemeFactory();
                    _model.WindowTheme = UIManager.WindowTheme.LabTheme;
                    break;
                case UIManager.WindowTheme.SpaceTheme:
                    factory = new SpaceThemeFactory();
                    _model.WindowTheme = UIManager.WindowTheme.SpaceTheme;
                    break;
                default:
                    return;
            }

            ISprite sprite = factory.CreateSprite();
            _uiManager.DrawSprite(xCoord, yCoord, sprite.GetSprite());
        }
        private void DefineAndDrawBackground()
        {
            IThemeFactory factory;
            switch (_model.WindowTheme)
            {
                case WindowTheme.LabTheme:
                    factory = new LabThemeFactory();
                    break;
                case WindowTheme.SpaceTheme:
                    factory = new SpaceThemeFactory();
                    break;
                default:
                    return;
            }

            IBackground background = factory.CreateBackground();
            _uiManager.SetBackground(background);
        }
        private Grid GetGridForLevel(int levelId)
        {
            GridDirector director = new GridDirector();
            SimpleGridBuilder builder = new SimpleGridBuilder();
            director.Builder = builder;

            switch (levelId)
            {
                case 1:
                    director.BuildGridForLevel1();
                    _model.CurrentLevelId = 1;
                    break;
                case 2:
                    director.BuildGridForLevel2();
                    _model.CurrentLevelId = 2;
                    break;
            }

            var grid = builder.BuildGrid();

            if (_coinsManager.GetCoinsNumber() == 10)
            {
                //DoubleCoinsDecorator should be here
            }
            else if (_coinsManager.GetCoinsNumber() == 50)
            { 
                //BonusLevelDecorator should be here 
            }
            else if (_coinsManager.GetCoinsNumber() == 100)
            {
                //Both decorators should be here
            }

            return grid;
        }
    }
}