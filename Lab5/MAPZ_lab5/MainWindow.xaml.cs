﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MAPZ_lab5.Patterns;
using static MAPZ_lab5.UIManager;
using Block = MAPZ_lab5.Patterns.Grid.Block;
using Grid = MAPZ_lab5.Patterns.Grid;

namespace MAPZ_lab5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Facade facade;

        public MainWindow()
        {
            InitializeComponent();
            facade = new Facade(this);
            facade.InitializeComponents();
        }

        private void SwitchTheme_BtnClick(object sender, RoutedEventArgs e)
        {
            facade.SwitchThemes();
        }

        private void ChangeLevel_BtnClick(object sender, RoutedEventArgs e)
        {
            facade.ChangeLevels();
        }
    }

    public class UIManager
    {
        private MainWindow _mainWindow;

        public UIManager(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
        }

        public void SetBackground(IBackground background)
        {
            BitmapImage bitmapImage = new BitmapImage(new Uri(background.GetBackground(), UriKind.Relative));
            ImageBrush imageBrush = new ImageBrush(bitmapImage);
            _mainWindow.Background = imageBrush;
        }

        public void DrawGrid(Grid builtGrid)
        {
            _mainWindow.UIGrid.Children.Clear();
            _mainWindow.UIGrid.RowDefinitions.Clear();
            _mainWindow.UIGrid.ColumnDefinitions.Clear();

            for (int row = 0; row < builtGrid.Width; row++)
            {
                _mainWindow.UIGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(70) });
            }

            for (int column = 0; column < builtGrid.Height; column++)
            {
                _mainWindow.UIGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(70) });
            }

            for (int row = 0; row < builtGrid.Width; row++)
            {
                for (int column = 0; column < builtGrid.Height; column++)
                {
                    Canvas canvas = new Canvas();
                    canvas.HorizontalAlignment = HorizontalAlignment.Stretch;
                    canvas.VerticalAlignment = VerticalAlignment.Stretch;

                    Block newBlock = builtGrid.GetBlock(row, column);
                    ChooseBlockColor(canvas, newBlock);
                    canvas.Margin = new Thickness(2);

                    System.Windows.Controls.Grid.SetColumn(canvas, column);
                    System.Windows.Controls.Grid.SetRow(canvas, row);
                    _mainWindow.UIGrid.Children.Add(canvas);

                    if (newBlock != null)
                    {
                        if (newBlock.CoinsCount == 1 || newBlock.CoinsCount == 2)
                        {
                            DrawCoins(canvas, builtGrid, row, column);
                        }
                    }
                }
            }
        }

        public void DrawSprite(int row, int column, string imagePath)
        {
            if (row < 0 || row >= _mainWindow.UIGrid.RowDefinitions.Count ||
                column < 0 || column >= _mainWindow.UIGrid.ColumnDefinitions.Count)
            {
                throw new ArgumentException("Cell with given coords doesn't exist.");
            }

            Canvas canvas = FindCanvasInGrid(_mainWindow.UIGrid, row, column);
            if (canvas != null)
            {
                ClearCell(row, column);
                Image image = new Image();
                image.Stretch = Stretch.Fill;
                image.Margin = new Thickness(5);
                image.Source = new BitmapImage(new Uri(imagePath, UriKind.Absolute));
                image.Height = 40;
                image.Width = 40;
                image.Tag = "sprite";

                canvas.Children.Add(image);
                Canvas.SetLeft(image, 10);
                Canvas.SetTop(image, 10);
            }
            else
            {
                throw new InvalidOperationException("No Canvas found at the specified cell.");
            }
        }

        public void DrawCoins(Canvas canvas, Grid builtGrid, int row, int column)
        {
            Image image = new Image();
            image.Stretch = Stretch.Fill;
            image.Margin = new Thickness(5);

            image.Source =
                new BitmapImage(new Uri(builtGrid.GetBlock(row, column).CoinsCount == 1 ? CoinPath : DoubleCoinPath,
                    UriKind.Absolute));
            image.Height = 20;
            image.Width = 20;
            image.Tag = "doubleCoin";
            canvas.Children.Add(image);

            Canvas.SetLeft(image, 17);
            Canvas.SetTop(image, 17);
        }

        public void ChooseBlockColor(Canvas canvas, Block block)
        {
            if (block == null)
            {
                canvas.Background = Brushes.Transparent;
            }
            else if (block.Type == BlockType.Blue)
            {
                canvas.Background = new SolidColorBrush(Color.FromRgb(0x4d, 0x61, 0xf7));
            }
            else if (block.Type == BlockType.Gray)
            {
                canvas.Background = Brushes.Beige;
            }
        }

        public Canvas FindCanvasInGrid(System.Windows.Controls.Grid grid, int row, int column)
        {
            foreach (UIElement uie in grid.Children)
            {
                if (System.Windows.Controls.Grid.GetColumn(uie) == column &&
                    System.Windows.Controls.Grid.GetRow(uie) == row &&
                    uie is Canvas canvas)
                {
                    return canvas;
                }
            }

            return null;
        }

        public void ClearCell(int row, int column)
        {
            Canvas canvas = FindCanvasInGrid(_mainWindow.UIGrid, row, column);
            if (canvas != null)
            {
                canvas.Children.Clear();
            }
        }

        // Enums and constants
        public enum WindowTheme
        {
            LabTheme,
            SpaceTheme
        }

        public const string CoinPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab5\\buttonsIco\\coin.png";
        public const string DoubleCoinPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab5\\buttonsIco\\doubleCoin.png";
    }

    public class GameModel
    {
        public WindowTheme WindowTheme;
        public int CurrentLevelId;
        public List<Level> levelsList;

        public GameModel()
        {
            levelsList = new List<Level>();
        }

        public void AddLevel(Level newLevel)
        {
            if (levelsList != null)
            {
                levelsList.Add(newLevel);
            }
        }

        public void CreateLevel(Grid grid)
        {
            int newLevelId = (levelsList == null) ? 1 : levelsList.Count();
            Level newLevel = new Level(grid, newLevelId);
            this.AddLevel(newLevel);
        }
    }

    public class Level
    {
        private readonly int _id;
        private Grid _grid;
        private readonly int _requiredBlocksToLight;
        private int _lightedBlocksCount = 0;

        public Grid LevelGrid => _grid;

        public Level(Grid grid, int id)
        {
            if (grid == null)
            {
                throw new Exception("This grid has no blocks");
            }

            _grid = grid;
            _id = id;
            _requiredBlocksToLight = _grid.GetNumberOfBlocksToLight();
        }

        public void LightBlock()
        {
            ++_lightedBlocksCount;
            if (_lightedBlocksCount == _requiredBlocksToLight)
            {
                MessageBox.Show("You passed the level. Take your +10 coins.", "Congratulation!",
                    MessageBoxButton.OK, MessageBoxImage.Information);

                CoinsManager coinsManager = new CoinsManager();
                coinsManager.AddCoins(10);
            }
        }

        public int GetId()
        {
            return _id;
        }

        public override string ToString()
        {
            return $"Level {_id}: {_lightedBlocksCount}/{_requiredBlocksToLight} blocks lit";
        }
    }
}