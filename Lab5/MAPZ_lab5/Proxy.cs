﻿using MAPZ_lab5.Patterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab5.Patterns
{
    public interface ICoinsManager
    {
        void AddCoins(int amount);
        void RemoveCoins(int amount);
        int GetCoinsNumber();
    }

    public class Proxy : ICoinsManager
    {
        private ICoinsManager _coinsManager;

        public Proxy()
        {
            this._coinsManager = new CoinsManager();
        }
        public bool CheckCoinsNumber()
        {
            int coinsNumber = _coinsManager.GetCoinsNumber();
            Log($"Checking coins balance. Current coins balance is {coinsNumber}");
            return coinsNumber > 0;
        }
        public void Log(string message)
        {
            Console.WriteLine($"LOG: {message}");
        }
        public void AddCoins(int amount)
        {
            _coinsManager.AddCoins(amount);
            Log($"Coins balance was increased by {amount} coin(-s). Current coins balance is {_coinsManager.GetCoinsNumber()}");
        }
        public void RemoveCoins(int amount)
        {
            if (CheckCoinsNumber())
            {
                _coinsManager.RemoveCoins(amount);
                Log($"Coins balance was decreased by {amount} coin(-s). Current coins balance is {_coinsManager.GetCoinsNumber()}");
            }
        }
        public int GetCoinsNumber()
        {
            return _coinsManager.GetCoinsNumber();
        }
    }

    public class CoinsManager : ICoinsManager
    {
        private int _coinsCount;

        public CoinsManager()
        {
            _coinsCount = 0;
        }
        public void AddCoins(int amount)
        {
            _coinsCount += amount;
        }
        public void RemoveCoins(int amount)
        {
            _coinsCount -= amount;
        }
        public int GetCoinsNumber()
        {
            return _coinsCount;
        }
    }
}
