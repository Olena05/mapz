﻿using MAPZ_lab5.Patterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Block = MAPZ_lab5.Patterns.Grid.Block;

namespace MAPZ_lab5.Patterns
{
    public interface IGrid
    {
        int Width { get; }
        int Height { get; }
        void SetBlockToPosition(int x, int y, BlockType type);
        Block GetBlock(int x, int y);
        Block[,] GetAllBlocks();
    }

    public class Grid : IGrid
    {
        private Block[,] _blocks;
        private int _levelId;
        public const int DefaultWidth = 1;
        public const int DefaultHeight = 1;

        public int Width => _blocks.GetLength(0);
        public int Height => _blocks.GetLength(1);

        public Grid(int width = 1, int height = 1)
        {
            _blocks = new Block[width, height];
        }
        public int GetNumberOfBlocksToLight()
        {
            int count = 0;
            foreach (var block in _blocks)
            {
                if (block != null && block.Type == BlockType.Blue)
                {
                    ++count;
                }
            }
            return count;
        }
        public void SetBlockToPosition(int x, int y, BlockType type)
        {
            if (x >= 0 && y >= 0 && x < Width && y < Height)
            {
                _blocks[x, y] = new Block(type);
            }
        }
        public Block GetBlock(int x, int y)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
            {
                return null;
            }
            return _blocks[x, y];
        }
        public Block[,] GetAllBlocks()
        {
            return _blocks;
        }
        public class Block
        {
            private BlockType _type;
            private int _coinsCount;

            public BlockType Type => _type;
            public int CoinsCount => _coinsCount;

            public Block(BlockType type)
            {
                _type = type;
                _coinsCount = 0;
            }

            public void SetCoins(int coinsNumber)
            {
                _coinsCount = coinsNumber;
            }
        }
    }

    public enum BlockType
    {
        Gray, // default block
        Blue // block to light
    }

    public abstract class GridDecorator : IGrid
    {
        protected IGrid _grid;
        public GridDecorator(IGrid grid)
        {
            _grid = grid;
        }
        public int Width => _grid.Width;
        public int Height => _grid.Height;
        public void SetBlockToPosition(int x, int y, BlockType type) => _grid.SetBlockToPosition(x, y, type);
        public Block GetBlock(int x, int y) => _grid.GetBlock(x, y);
        public Block[,] GetAllBlocks() => _grid.GetAllBlocks();
    }

    public class BonusLevelDecorator : GridDecorator
    {
        public BonusLevelDecorator(IGrid grid)
            : base(grid)
        {

        }
        public Block GetBlock(int x, int y)
        {
            var old = GetBlock(x, y);
            var decorated = new Block(old.Type);
            decorated.SetCoins(2);
            return decorated;
        }
    }

    public class DoubleCoinsDecorator : GridDecorator
    {
        public DoubleCoinsDecorator(IGrid grid)
            : base(grid)
        {

        }
        public Block GetBlock(int x, int y)
        {
            var old = GetBlock(x, y);
            var decorated = new Block(old.Type);
            if (old.Type == BlockType.Gray)
            {
                decorated.SetCoins(1);
            }
            return decorated;
        }
    }
}
