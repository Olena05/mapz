﻿using System;
using System.Collections.Generic;

namespace MAPZ_lab4.Patterns
{
    public interface IThemeFactory
    {
        ISprite CreateSprite();
        IBackground CreateBackground();
    }

    public class LabThemeFactory : IThemeFactory
    {
        public ISprite CreateSprite()
        {
            Console.WriteLine("The laboratory sprite was created");
            return new LabSprite();
        }

        public IBackground CreateBackground()
        {
            Console.WriteLine("The laboratory background was created");
            return new LabBackground();
        }
    }

    public class SpaceThemeFactory : IThemeFactory
    {
        public ISprite CreateSprite()
        {
            Console.WriteLine("The space sprite was created");
            return new SpaceSprite();
        }

        public IBackground CreateBackground()
        {
            Console.WriteLine("The space background was created");
            return new SpaceBackground();
        }
    }

    public interface ISprite
    {
        string GetSprite();
    }

    public class LabSprite : ISprite
    {
        public string GetSprite()
        {
            return "C:\\Users\\olena\\source\\repos\\MAPZ_lab4\\sprites\\robot.png";
        }
        public override string ToString()
        {
            return "LabSprite";
        }
    }

    public class SpaceSprite : ISprite
    {
        public string GetSprite()
        {
            return "C:\\Users\\olena\\source\\repos\\MAPZ_lab4\\sprites\\astronaut.png";
        }
        public override string ToString()
        {
            return "SpaceSprite";
        }
    }

    public interface IBackground
    {
        string GetBackground();
    }

    public class LabBackground : IBackground
    {
        public string GetBackground()
        {
            return "C:\\Users\\olena\\source\\repos\\MAPZ_lab4\\backgrounds\\labBackground.jpg";
        }
        public override string ToString()
        {
            return "LabBackground";
        }
    }

    public class SpaceBackground : IBackground
    {
        public string GetBackground()
        {
            return "C:\\Users\\olena\\source\\repos\\MAPZ_lab4\\backgrounds\\spaceBackground.jpg";
        }
        public override string ToString()
        {
            return "SpaceBackground";
        }
    }

    public class ThemeFactory
    {
        private IThemeFactory _themeFactory;

        public ThemeFactory(IThemeFactory factory)
        {
            _themeFactory = factory;
        }

        public ISprite CreateSprite()
        {
            return _themeFactory.CreateSprite();
        }

        public IBackground CreateBackground()
        {
            return _themeFactory.CreateBackground();
        }
    }
}