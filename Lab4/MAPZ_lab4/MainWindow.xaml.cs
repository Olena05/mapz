﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using MAPZ_lab4.Patterns;
using Block = MAPZ_lab4.Patterns.Grid.Block;
using Grid = MAPZ_lab4.Patterns.Grid;

namespace MAPZ_lab4
{
    public partial class MainWindow : Window
    {
        CoinsManager coinsManager = CoinsManager.GetInstance;
        GameModel model = new GameModel();

        public MainWindow()
        {
            InitializeComponent();
            model.CreateLevel(GetGridForLevel(1));
            DrawGrid(model.levelsList[0].LevelGrid);
            SetLevelTheme(WindowTheme.LabTheme);
        }

        // Main game-manager
        class GameModel
        {
            public WindowTheme WindowTheme;
            public int CurrentLevelId;
            public List<Level> levelsList;

            public GameModel()
            {
                levelsList = new List<Level>();
            }

            public void AddLevel(Level newLevel)
            {
                if (levelsList != null)
                {
                    levelsList.Add(newLevel);
                }
            }

            public void CreateLevel(Grid grid)
            {
                int newLevelId = (levelsList == null) ? 1 : levelsList.Count();
                Level newLevel = new Level(grid, newLevelId);
                this.AddLevel(newLevel);
            }
        }

        public class Level
        {
            private readonly int _id;
            private Grid _grid;
            private readonly int _requiredBlocksToLight;
            private int _lightedBlocksCount = 0;

            public Grid LevelGrid => _grid;
            public Level(Grid grid, int id)
            {
                if (grid == null)
                {
                    throw new Exception("This grid has no blocks");
                }

                this._grid = grid;
                this._id = id;
                this._requiredBlocksToLight = _grid.GetNumberOfBlocksToLight();
            }

            public void LightBlock()
            {
                ++_lightedBlocksCount;
                if (_lightedBlocksCount == _requiredBlocksToLight)
                {
                    MessageBox.Show("You passed the level. Take your +10 coins.", "Congratulation!",
                        MessageBoxButton.OK, MessageBoxImage.Information);

                    CoinsManager coinsManager = CoinsManager.GetInstance;
                    coinsManager.AddCoins(10);
                }
            }

            public int GetId()
            {
                return _id;
            }

            public override string ToString()
            {
                return $"Level {_id}: {_lightedBlocksCount}/{_requiredBlocksToLight} blocks lit";
            }
        }

        // Patterns-related
        private void SwitchTheme_BtnClick(object sender, RoutedEventArgs e)
        {
            switch (model.WindowTheme)
            {
                case WindowTheme.LabTheme:
                    model.WindowTheme = WindowTheme.SpaceTheme;
                    break;
                case WindowTheme.SpaceTheme:
                    model.WindowTheme = WindowTheme.LabTheme;
                    break;
            }

            DefineAndDrawBackground();
            DeineAndDrawSprite(0, 0);
        }

        private void SetLevelTheme(WindowTheme theme)
        {
            switch (theme)
            {
                case WindowTheme.LabTheme:
                    model.WindowTheme = WindowTheme.LabTheme;
                    break;
                case WindowTheme.SpaceTheme:
                    model.WindowTheme = WindowTheme.SpaceTheme;
                    break;
            }

            DefineAndDrawBackground();
            DeineAndDrawSprite(0, 0);
        }

        private void ChangeLevel_BtnClick(object sender, RoutedEventArgs e)
        {
            int levelId = (model.CurrentLevelId == 1) ? 2 : 1;
            if (levelId > model.levelsList.Count - 1)
            {
                model.CreateLevel(GetGridForLevel(levelId));
            }
            DrawGrid(model.levelsList[levelId-1].LevelGrid);
            model.CurrentLevelId = levelId;
        }
        public void DefineAndDrawBackground()
        {
            IThemeFactory factory;
            switch (model.WindowTheme)
            {
                case WindowTheme.LabTheme:
                    factory = new LabThemeFactory();
                    break;
                case WindowTheme.SpaceTheme:
                    factory = new SpaceThemeFactory();
                    break;
                default:
                    return;
            }
            IBackground background = factory.CreateBackground();
            BitmapImage bitmapImage = new BitmapImage(new Uri(background.GetBackground(), UriKind.Relative));
            ImageBrush imageBrush = new ImageBrush(bitmapImage);
            this.Background = imageBrush;
        }

        private void DeineAndDrawSprite(int xCoord, int yCoord)
        {
            IThemeFactory factory;
            switch (model.WindowTheme)
            {
                case WindowTheme.LabTheme:
                    factory = new LabThemeFactory();
                    model.WindowTheme = WindowTheme.LabTheme;
                    break;
                case WindowTheme.SpaceTheme:
                    factory = new SpaceThemeFactory();
                    model.WindowTheme = WindowTheme.SpaceTheme;
                    break;
                default:
                    return;
            }

            ISprite sprite = factory.CreateSprite();
            DrawSprite(xCoord, yCoord, sprite.GetSprite());
        }

        private Grid GetGridForLevel(int levelId)
        {
            GridDirector director = new GridDirector();
            SimpleGridBuilder builder = new SimpleGridBuilder();
            director.Builder = builder;

            switch (levelId)
            {
                case 1:
                    director.BuildGridForLevel1();
                    model.CurrentLevelId = 1;
                    break;
                case 2:
                    director.BuildGridForLevel2();
                    model.CurrentLevelId = 2;
                    break;
            }

            var grid = builder.BuildGrid();
            return grid;
        }

        //XAML-related

        public void DrawGrid(Grid builtGrid)
        {
            UIGrid.Children.Clear();
            UIGrid.RowDefinitions.Clear();
            UIGrid.ColumnDefinitions.Clear();

            for (int row = 0; row < builtGrid.Width; row++)
            {
                UIGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(70) });
            }

            for (int column = 0; column < builtGrid.Height; column++)
            {
                UIGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(70) });
            }

            for (int row = 0; row < builtGrid.Width; row++)
            {
                for (int column = 0; column < builtGrid.Height; column++)
                {
                    Canvas canvas = new Canvas();
                    canvas.HorizontalAlignment = HorizontalAlignment.Stretch;
                    canvas.VerticalAlignment = VerticalAlignment.Stretch;

                    Block newBlock = builtGrid.GetBlock(row, column);
                    ChooseBlockColor(canvas, newBlock);
                    canvas.Margin = new Thickness(2);

                    System.Windows.Controls.Grid.SetColumn(canvas, column);
                    System.Windows.Controls.Grid.SetRow(canvas, row);
                    UIGrid.Children.Add(canvas);

                    if (newBlock != null)
                    {
                        if (newBlock.CoinsCount == 1 || newBlock.CoinsCount == 2)
                        {
                            DrawCoins(canvas, builtGrid, row, column);
                        }
                    }
                }
            }
            DeineAndDrawSprite(0, 0);
        }

        private void DrawSprite(int row, int column, string imagePath)
        {
            if (row < 0 || row >= UIGrid.RowDefinitions.Count ||
                column < 0 || column >= UIGrid.ColumnDefinitions.Count)
            {
                throw new ArgumentException("Cell with given coords doesn't exist.");
            }

            Canvas canvas = FindCanvasInGrid(UIGrid, row, column);
            if (canvas != null)
            {
                ClearCell(row, column);
                Image image = new Image();
                image.Stretch = Stretch.Fill;
                image.Margin = new Thickness(5);
                image.Source = new BitmapImage(new Uri(imagePath, UriKind.Absolute));
                image.Height = 40;
                image.Width = 40;
                image.Tag = "sprite";

                canvas.Children.Add(image);
                Canvas.SetLeft(image, 10);
                Canvas.SetTop(image, 10);
            }
            else
            {
                throw new InvalidOperationException("No Canvas found at the specified cell.");
            }
        }

        private void DrawCoins(Canvas canvas, Grid builtGrid, int row, int column)
        {
            Image image = new Image();
            image.Stretch = Stretch.Fill;
            image.Margin = new Thickness(5);

            image.Source =
                new BitmapImage(new Uri(builtGrid.GetBlock(row, column).CoinsCount == 1 ? CoinPath : DoubleCoinPath, UriKind.Absolute));
            image.Height = 20;
            image.Width = 20;
            image.Tag = "doubleCoin";
            canvas.Children.Add(image);

            Canvas.SetLeft(image, 17);
            Canvas.SetTop(image, 17);
        }

        private void ChooseBlockColor(Canvas canvas, Block block)
        {
            if (block == null)
            {
                canvas.Background = Brushes.Transparent;
            }
            else if (block.Type == BlockType.Blue)
            {
                canvas.Background = new SolidColorBrush(Color.FromRgb(0x4d, 0x61, 0xf7));
            }
            else if (block.Type == BlockType.Gray)
            {
                canvas.Background = Brushes.Beige;
            }
        }

        private Canvas FindCanvasInGrid(System.Windows.Controls.Grid grid, int row, int column)
        {
            foreach (UIElement uie in grid.Children)
            {
                if (System.Windows.Controls.Grid.GetColumn(uie) == column &&
                    System.Windows.Controls.Grid.GetRow(uie) == row &&
                    uie is Canvas canvas)
                {
                    return canvas;
                }
            }
            return null;
        }

        private void ClearCell(int row, int column)
        {
            Canvas canvas = FindCanvasInGrid(UIGrid, row, column);
            if (canvas != null)
            {
                canvas.Children.Clear();
            }
        }

        // Enums and parsers
        private BackgroundType ParseBackgroundType(string? background)
        {
            switch (background)
            {
                case "LabBackground":
                    return BackgroundType.LabBackground;
                    break;
                case "SpaceBackground":
                    return BackgroundType.SpaceBackground;
                default:
                    throw new ArgumentException("Invalid background type.");
            }
        }

        private enum WindowTheme
        {
            LabTheme,
            SpaceTheme
        }
        private enum BackgroundType
        {
            LabBackground,
            SpaceBackground
        }

        public const string CoinPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab4\\buttonsIco\\coin.png";
        public const string DoubleCoinPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab4\\buttonsIco\\doubleCoin.png";
    }
}