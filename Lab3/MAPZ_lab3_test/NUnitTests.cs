﻿// Don't forget to install all necessary packages
// Which ones are necessary read in packages.config
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using LevelExtensions;
using LevelsListExtensions;
using MAPZ_lab3;
using NUnit.Framework.Constraints;

namespace MAPZ_lab3_test
{
    [TestFixture]
    public class NUnitTests
    {
        private ModelTests model;

        [SetUp]
        public void Setup()
        {
            model = new ModelTests();
        }

        // FIRST PART

        [Test] 
        public void TestSelectQuery() 
        {
            var arr = model.Levels.Select(level => level.Name).ToArray();
            Assert.That(arr[arr.Length-1], Is.EqualTo(model.Levels[model.Levels.Count - 1].Name));
        }

        [Test]
        public void TestWhereQuery()
        {
            var arr = model.Levels.Where(level => level.LightedTilesCount == 0).ToArray();

            var levelsWithoutLightedTiles = model.Levels.Count(level => level.LightedTilesCount == 0);
            Assert.That(arr.Length, Is.EqualTo(levelsWithoutLightedTiles));
        }

        [Test]
        public void TestListOperations()
        {
            var levels = model.Levels;
            levels.Sort((level1, level2) => level1.Name.CompareTo(level2.Name));
            Assert.That(levels[0].Name, Is.EqualTo("Advanced Puzzle"));
        }

        [Test]
        public void TestDictionaryOperations()
        {
            var categoriesDict = model.Categories.ToDictionary(
                category => category.Id,
                category => category
            );

            var existingKey = model.Categories[0].Id;
            var nonExistingKey = new Category("nonExistingValue").Id;
            Assert.That(categoriesDict.ContainsKey(existingKey), Is.True); 
            Assert.That(categoriesDict.ContainsKey(nonExistingKey), Is.False);
        }

        [Test]
        public void TestExtensionMethod()
        {
            var hardLevel = new Level(name: "Advanced", category: model.Categories[4], tilesToLight: 4) { LightedTilesCount = 0 };
            var mediumLevel = new Level(name: "Not too hard", category: model.Categories[2], tilesToLight: 6) { LightedTilesCount = 3 };
            var easyLevel = new Level(name: "Easy one", category: model.Categories[0], tilesToLight: 8) { LightedTilesCount = 8 };

            Assert.That(hardLevel.GetCompletionPercentag(), Is.EqualTo(0));
            Assert.That(mediumLevel.GetCompletionPercentag(), Is.EqualTo(50));
            Assert.That(easyLevel.GetCompletionPercentag(), Is.EqualTo(100));
        }

        [Test]
        public void TestAnonymous()
        {
            var anonimousLevel = new { category = model.Categories[3], tilesToLight = 4, LightedTilesCount = 2 };
            var anonimousLevelsArray = new[] {
                new { category = model.Categories[3], tilesToLight = 4, LightedTilesCount = 2 },
                new { category = model.Categories[2], tilesToLight = 5, LightedTilesCount = 3 }
            };
            
            Assert.That(anonimousLevel, Is.EqualTo(anonimousLevelsArray[0]));
        }

        [Test]
        public void TestCompaison()
        {
            var comparator = new LevelsComparator();

            var sortedLevels = model.Levels.OrderBy(level => level, comparator).ToList();

            for (int i = 0; i < sortedLevels.Count - 1; i++)
            {
                Assert.That(comparator.Compare(sortedLevels[i], sortedLevels[i + 1]), Is.LessThanOrEqualTo(0));
            }
        }

        [Test]
        public void TestToArray()
        {
            var simplerCategories = model.Categories.Where(category => category.Name != "Challenges").ToArray();
            Assert.That(simplerCategories.Length, Is.EqualTo(model.Categories.Count - 1));
        }

        [Test]
        public void TestSorting()
        {
            var levelsSortedByName = model.Levels
                .OrderBy(level => level.Name)
                .Select(level => level.Name);

            Assert.That(levelsSortedByName.Count(), Is.EqualTo(model.Levels.Count));
            Assert.That(levelsSortedByName, Is.Ordered);
        }

        // SECOND PART

        [Test]
        public void TestLevelsDictionary()
        {
            var levelsMap = model.Levels.ToDictionary(
                level => level.LevelId,
                level => level
            );

            Assert.That(levelsMap.Count, Is.EqualTo(model.Levels.Count));
            foreach (var level in model.Levels)
            {
                Assert.That(levelsMap.ContainsKey(level.LevelId), Is.True);
            }
        }

        [Test]
        public void TestCategoriesDictionary()
        {
            var categoriesMap = model.Categories.ToDictionary(
                category => category.Id,
                category => category
            );

            Assert.That(categoriesMap.Count, Is.EqualTo(model.Categories.Count));
            foreach (var category in model.Categories)
            {
                Assert.That(categoriesMap.ContainsKey(category.Id), Is.True);
            }
        }

        [Test]
        public void TestLevelsSortedList()
        {
            var levelsNamesSortedList = model.Levels.ToSortedList(level => level.Name);

            Assert.That(levelsNamesSortedList.Keys, Is.Ordered);
            Assert.That(levelsNamesSortedList.Count, Is.EqualTo(model.Levels.Count));

            var levelsIdsSortedList = model.Levels.ToSortedList(level => level.LevelId);

            Assert.That(levelsIdsSortedList.Keys, Is.Ordered);
            Assert.That(levelsIdsSortedList.Count, Is.EqualTo(model.Levels.Count));
        }

        [Test]
        public void TestLevelsQueue()
        {
            var levelsQueue = new Queue<Level>(model.Levels);

            Assert.That(levelsQueue.Count, Is.EqualTo(model.Levels.Count));

            Assert.That(levelsQueue.Peek().Name, Is.EquivalentTo(model.Levels[0].Name));
            levelsQueue.Dequeue();
            Assert.That(levelsQueue.Peek().Name, Is.EquivalentTo(model.Levels[1].Name));

            Assert.That(levelsQueue.Count, Is.EqualTo(model.Levels.Count - 1));
        }

        [Test]
        public void TestLevelsStack()
        {
            var levelsNamesStack = model.Levels.ToStack(level => level.Name);

            Assert.That(levelsNamesStack.Count, Is.EqualTo(model.Levels.Count));
            Assert.That(levelsNamesStack.Pop(), Is.EqualTo(model.Levels[model.Levels.Count - 1].Name));
            Assert.That(levelsNamesStack.Pop(), Is.EqualTo(model.Levels[model.Levels.Count - 2].Name));
            Assert.That(levelsNamesStack.Count, Is.EqualTo(model.Levels.Count - 2));

            var levelsIdsStack = model.Levels.ToStack(level => level.LevelId);

            Assert.That(levelsIdsStack.Count, Is.EqualTo(model.Levels.Count));
            Assert.That(levelsIdsStack.Pop(), Is.EqualTo(model.Levels[model.Levels.Count - 1].LevelId));
            Assert.That(levelsIdsStack.Pop(), Is.EqualTo(model.Levels[model.Levels.Count - 2].LevelId));
            Assert.That(levelsIdsStack.Count, Is.EqualTo(model.Levels.Count - 2));
        }

        [Test]
        public void TestGroupBy()
        {
            var levelsGroupedById = model.Levels.GroupBy(level => level.CategoryId);
            Assert.That(levelsGroupedById.Count(), Is.EqualTo(model.Categories.Count()));
        }

        [Test]
        public void TestComplicatedOperations()
        {
            var levelsSortedByTilesToLight = model.Levels
                .Where(level => level.LightedTilesCount != 0) 
                .OrderByDescending(level => level.LightedTilesCount) 
                .Select(level => (Name: level.Name, LightedTilesCount: level.LightedTilesCount)); 

            Assert.That(levelsSortedByTilesToLight, Is.Not.Null); 
            Assert.That(levelsSortedByTilesToLight.Select(level => level.LightedTilesCount), Is.Ordered.Descending);
        }
    }
}