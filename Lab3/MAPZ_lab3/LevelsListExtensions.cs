﻿using System;
using System.Collections.Generic;

namespace LevelsListExtensions
{
    public static class LevelsListExtensions
    {
        public static SortedList<TLevelProperty, TLevel> ToSortedList<TLevelProperty, TLevel>
            (this List<TLevel> levelsList, Func<TLevel, TLevelProperty> selector) 
        {
            var sortedLevelList = new SortedList<TLevelProperty, TLevel>();
            levelsList.ForEach(level => sortedLevelList.Add(selector(level), level));
            return sortedLevelList;
        }

        public static Stack<TLevelProperty> ToStack<TLevel, TLevelProperty>
            (this List<TLevel> levelsList, Func<TLevel, TLevelProperty> selector)
        {
            var levelsStack = new Stack<TLevelProperty>();
            levelsList.ForEach(level => levelsStack.Push(selector(level)));
            return levelsStack;
        }
    }
}