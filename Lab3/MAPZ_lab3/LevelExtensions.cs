﻿using MAPZ_lab3;

namespace LevelExtensions
{
    public static class LevelExtensions
    {
        public static double GetCompletionPercentag(this Level level)
        {
            var persantage = (double)level.LightedTilesCount / level.TilesToLight * 100;
            return persantage;
        }
    }
}