﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_lab3
{
    public class ModelTests
    {
        private List<Category> categories;
        private List<Level> levels; 

        public ModelTests()
        {
            categories = new List<Category>()
            {
                new Category(name: LevelsCategoriesNames.Basics.ToString()),
                new Category(name: LevelsCategoriesNames.Conditions.ToString()),
                new Category(name: LevelsCategoriesNames.Procedures.ToString()),
                new Category(name: LevelsCategoriesNames.Loops.ToString()),
                new Category(name: LevelsCategoriesNames.Challenges.ToString())
            };

            levels = new List<Level>()
            {
                new Level(name: "Get Started", category: categories[0], tilesToLight: 1) { LightedTilesCount = 1 },
                new Level(name: "Building Blocks", category: categories[0], tilesToLight: 3) { LightedTilesCount = 3 },
                new Level(name: "Making Decisions", category: categories[1], tilesToLight: 5),
                new Level(name: "Branching Out", category: categories[1], tilesToLight: 6) { LightedTilesCount = 4 },
                new Level(name: "Breaking Down Tasks", category: categories[2], tilesToLight: 5) { LightedTilesCount = 5 },
                new Level(name: "Modular Programming", category: categories[2], tilesToLight: 7) { LightedTilesCount = 6 },
                new Level(name: "Repeating Actions", category: categories[3], tilesToLight: 4),
                new Level(name: "Automating Tasks", category: categories[3], tilesToLight: 8) { LightedTilesCount = 7 },
                new Level(name: "Advanced Puzzle", category: categories[4], tilesToLight: 9) { LightedTilesCount = 5 },
                new Level(name: "Logic Legends", category: categories[4], tilesToLight: 12) { LightedTilesCount = 2 },
            };
        }

        public List<Category> Categories => categories;
        public List<Level> Levels => levels;
        public void ShowLevelsList()
        {
            var strBuilder = new StringBuilder();

            Levels.ForEach(level =>
                strBuilder.Append(String.Format(
                    "Name: {0}, Category: {1}, Category name: {2}, Tiles to Light: {3}, Lighted Tiles Count: {4}\n",
                    level.Name,
                    level.CategoryId,
                    Categories.Find(category => category.Id == level.CategoryId)?.Name ?? "Unknown",
                    level.TilesToLight,
                    level.LightedTilesCount)
                ));

            Console.WriteLine(strBuilder.ToString());
        }
    }

    public class LevelsComparator : IComparer<Level>
    {
        public int Compare(Level level1, Level level2)
        {
            int nameComparison = string.Compare(level1?.Name, level2?.Name, StringComparison.Ordinal);
            return nameComparison != 0 ? nameComparison : (level1?.LevelId.CompareTo(level2?.LevelId) ?? 0);
        }
    }

    public enum LevelsCategoriesNames
    {
        Basics,
        Conditions,
        Procedures,
        Loops,
        Challenges
    }
}