﻿using System;

namespace MAPZ_lab3
{
    public class Level
    {
        public Level(string name, Category category, int tilesToLight, int lightedTilesCount = 0)
        {
            Name = name;
            LevelId = Guid.NewGuid();
            CategoryId = category.Id;
            TilesToLight = tilesToLight;
            LightedTilesCount = lightedTilesCount;
        }

        public string Name { get; }
        public Guid LevelId { get; }
        public Guid CategoryId { get; }
        public int TilesToLight { get; }
        public int LightedTilesCount { get; set; }

        public void IncLightedTilesCount()
        {
            ++LightedTilesCount;
        }
    }
}