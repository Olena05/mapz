﻿using System;

namespace MAPZ_lab3
{
    public class Category
    {
        public Category(string name)
        {
            Name = name;
            Id = Guid.NewGuid();
        }

        public string Name { get; }
        public Guid Id { get; }
    }
}