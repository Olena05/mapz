﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MAPZ_lab6.Patterns;
using Block = MAPZ_lab6.Patterns.Grid.Block;
using Grid = MAPZ_lab6.Patterns.Grid;
using ICommand = MAPZ_lab6.Patterns.ICommand;

namespace MAPZ_lab6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CoinsManager _coinsManager = CoinsManager.GetInstance;
        private GameModel _model;
        private UIManager _uiManager;
        private MainWindowMediator _mediator;

        public WindowTheme ThemeFromStartWindow;
        public WindowMode ModeFromStartWindow;
        public List<Command> CommandsList;
        public StrategyContext Context;

        public bool IsFooStackPanelSelected => (FooStackPanel.Background as SolidColorBrush)?.Color == Colors.CornflowerBlue;

        public MainWindow(WindowTheme theme, WindowMode mode)
        {
            InitializeComponent();
            ThemeFromStartWindow = theme;
            ModeFromStartWindow = mode;

            _uiManager = new UIManager(this);
            _model = new GameModel();
            CommandsList = new List<Command>();

            _model.CreateLevel(GetGridForLevel(1));
            _uiManager.DrawGrid(_model.levelsList[0].LevelGrid);

            var themeSwitcher = new ThemeSwitcher();
            themeSwitcher.SetButtons(LabThemeBtn, SpaceThemeBtn);
            var modeSwitcher = new ModeSwitcher();
            modeSwitcher.SetButtons(EasyModeBtn, HardModeBtn);

            var mediator = new MainWindowMediator(this, _uiManager, themeSwitcher, modeSwitcher);

            SetInitialTheme(ThemeFromStartWindow);
            SetInitialMode(ModeFromStartWindow);
        }

        // Sprite commands buttons. Strategy pattern usage
        private void DoStepBtn_Click(object sender, RoutedEventArgs e)
        {
            Context.AddCommandToList(Command.DoStep);
        }
        private void TurnRightBtn_Click(object sender, RoutedEventArgs e)
        {
            Context.AddCommandToList(Command.TurnRight);
        }
        private void TurnLeftBtn_Click(object sender, RoutedEventArgs e)
        {
            Context.AddCommandToList(Command.TurnLeft);
        }
        private void LightBlockBtn_Click(object sender, RoutedEventArgs e)
        {
            Context.AddCommandToList(Command.LightBlock);
        }
        private void FooBtn_Click(object sender, RoutedEventArgs e)
        {
            Context.AddCommandToList(Command.Foo);
        }

        // Command pattern buttons
        private void CommandBtn_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            (button.Tag as ICommand).Execute();
        }

        // Main window navigation
        private void ChangeLevel_BtnClick(object sender, RoutedEventArgs e)
        {
            int levelId = (_model.CurrentLevelId == 1) ? 2 : 1;
            if (levelId > _model.levelsList.Count - 1)
            {
                _model.CreateLevel(GetGridForLevel(levelId));
            }

            _uiManager.DrawGrid(_model.levelsList[levelId - 1].LevelGrid);
            DefineAndDrawSprite(0, 0);
            _model.CurrentLevelId = levelId;

        }
        private void UndoBtn_Click(object sender, RoutedEventArgs e)
        {
            if (FooStackPanel.Background is SolidColorBrush brush &&
                brush.Color == Colors.CornflowerBlue)
            {
                if (FooStackPanel.Children.Count > 0 &&
                    FooStackPanel.Children[FooStackPanel.Children.Count - 1] is Label)
                {
                    FooStackPanel.Children.RemoveAt(FooStackPanel.Children.Count - 1);
                }
            }
            else
            {
                if (MainStackPanel.Children.Count > 0 &&
                    MainStackPanel.Children[MainStackPanel.Children.Count - 1] is Label)
                {
                    MainStackPanel.Children.RemoveAt(MainStackPanel.Children.Count - 1);
                }
            }
        }
        private void MainStackPan_Click(object sender, MouseButtonEventArgs e)
        {
            if (FooStackPanel.Visibility == Visibility.Visible)
            {
                MainStackPanel.Background = new SolidColorBrush(Colors.CornflowerBlue);
                FooStackPanel.Background = new SolidColorBrush(Colors.Beige);
            }
        }
        private void FooStackPan_Click(object sender, MouseButtonEventArgs e)
        {
            if (FooStackPanel.Visibility == Visibility.Visible)
            {
                MainStackPanel.Background = new SolidColorBrush(Colors.Beige);
                FooStackPanel.Background = new SolidColorBrush(Colors.CornflowerBlue);
            }
        }

        // Additional methods
        private Grid GetGridForLevel(int levelId)
        {
            GridDirector director = new GridDirector();
            GridBuilder builder = new GridBuilder();
            director.Builder = builder;

            switch (levelId)
            {
                case 1:
                    director.BuildEasyGridForLevel1();
                    _model.CurrentLevelId = 1;
                    break;
                case 2:
                    director.BuildEasyGridForLevel2();
                    _model.CurrentLevelId = 2;
                    break;
            }

            var grid = builder.BuildGrid();
            return grid;
        }
        public void SetInitialTheme(WindowTheme theme)
        {
            string backgroundPath;
            switch (theme)
            {
                case WindowTheme.Lab:
                    backgroundPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\backgrounds\\labBackground.jpg";
                    _model.WindowTheme = WindowTheme.Lab;
                    break;
                case WindowTheme.Space:
                    backgroundPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\backgrounds\\spaceBackground.jpg";
                    _model.WindowTheme = WindowTheme.Space;

                    SpaceThemeBtn.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
                    SpaceThemeBtn.Foreground = new SolidColorBrush(Colors.AliceBlue);
                    LabThemeBtn.Background = new SolidColorBrush(Colors.Transparent);
                    LabThemeBtn.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
                    break;
                default:
                    backgroundPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\backgrounds\\labBackground.jpg";
                    _model.WindowTheme = WindowTheme.Lab;
                    break;
            }
            _uiManager.SetBackground(backgroundPath);
            DefineAndDrawSprite(0, 0);
        }
        public void SetInitialMode(WindowMode theme)
        {
            IStrategy modeStrategy;
            switch (theme)
            {
                case WindowMode.Easy:
                    modeStrategy = new EasyModeStrategy();
                    break;
                case WindowMode.Hard:
                    modeStrategy = new HardModeStrategy();

                    HardModeBtn.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
                    HardModeBtn.Foreground = new SolidColorBrush(Colors.AliceBlue);
                    EasyModeBtn.Background = new SolidColorBrush(Colors.Transparent);
                    EasyModeBtn.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));

                    FooBtn.Visibility = Visibility.Visible;
                    FooStackPanel.Visibility = Visibility.Visible;
                    MainStackPanel.Height = 200;
                    FooStackPanel.Height = 200;
                    break;
                default:
                    modeStrategy = new EasyModeStrategy();
                    break;
            }
            Context = new StrategyContext(this);
            Context.SetStrategy(modeStrategy);
        }
        private void DefineAndDrawSprite(int xCoord, int yCoord)
        {
            string spritePath;
            switch (_model.WindowTheme)
            {
                case WindowTheme.Lab:
                    spritePath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\sprites\\robot.png";
                    break;
                case WindowTheme.Space:
                    spritePath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\sprites\\astronaut.png";
                    break;
                default:
                    spritePath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\sprites\\robot.png";
                    return;
            }
            _uiManager.DrawSprite(xCoord, yCoord, spritePath);
        }
    }

    public class UIManager
    {
        private MainWindow _mainWindow;

        public UIManager(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
        }
        public void SetBackground(string background)
        {
            BitmapImage bitmapImage = new BitmapImage(new Uri(background, UriKind.Relative));
            ImageBrush imageBrush = new ImageBrush(bitmapImage);
            _mainWindow.Background = imageBrush;
        }
        public void DrawGrid(Grid builtGrid)
        {
            _mainWindow.UIGrid.Children.Clear();
            _mainWindow.UIGrid.RowDefinitions.Clear();
            _mainWindow.UIGrid.ColumnDefinitions.Clear();

            for (int row = 0; row < builtGrid.Width; row++)
            {
                _mainWindow.UIGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(70) });
            }

            for (int column = 0; column < builtGrid.Height; column++)
            {
                _mainWindow.UIGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(70) });
            }

            for (int row = 0; row < builtGrid.Width; row++)
            {
                for (int column = 0; column < builtGrid.Height; column++)
                {
                    Canvas canvas = new Canvas();
                    canvas.HorizontalAlignment = HorizontalAlignment.Stretch;
                    canvas.VerticalAlignment = VerticalAlignment.Stretch;

                    Block newBlock = builtGrid.GetBlock(row, column);
                    ChooseBlockColor(canvas, newBlock);
                    canvas.Margin = new Thickness(2);

                    System.Windows.Controls.Grid.SetColumn(canvas, column);
                    System.Windows.Controls.Grid.SetRow(canvas, row);
                    _mainWindow.UIGrid.Children.Add(canvas);

                    if (newBlock != null)
                    {
                        if (newBlock.CoinsCount == 1 || newBlock.CoinsCount == 2)
                        {
                            DrawCoins(canvas, builtGrid, row, column);
                        }
                    }
                }
            }
        }
        public void DrawSprite(int row, int column, string imagePath)
        {
            if (row < 0 || row >= _mainWindow.UIGrid.RowDefinitions.Count ||
                column < 0 || column >= _mainWindow.UIGrid.ColumnDefinitions.Count)
            {
                throw new ArgumentException("Cell with given coords doesn't exist.");
            }

            Canvas canvas = FindCanvasInGrid(_mainWindow.UIGrid, row, column);
            if (canvas != null)
            {
                ClearCell(row, column);
                Image image = new Image();
                image.Stretch = Stretch.Fill;
                image.Margin = new Thickness(5);
                image.Source = new BitmapImage(new Uri(imagePath, UriKind.Absolute));
                image.Height = 40;
                image.Width = 40;
                image.Tag = "sprite";

                canvas.Children.Add(image);
                Canvas.SetLeft(image, 10);
                Canvas.SetTop(image, 10);
            }
            else
            {
                throw new InvalidOperationException("No Canvas found at the specified cell.");
            }
        }
        public void DrawCoins(Canvas canvas, Grid builtGrid, int row, int column)
        {
            Image image = new Image();
            image.Stretch = Stretch.Fill;
            image.Margin = new Thickness(5);

            image.Source =
                new BitmapImage(new Uri(builtGrid.GetBlock(row, column).CoinsCount == 1 ? CoinPath : DoubleCoinPath,
                    UriKind.Absolute));
            image.Height = 20;
            image.Width = 20;
            image.Tag = "doubleCoin";
            canvas.Children.Add(image);

            Canvas.SetLeft(image, 17);
            Canvas.SetTop(image, 17);
        }
        public void ChooseBlockColor(Canvas canvas, Block block)
        {
            if (block == null)
            {
                canvas.Background = Brushes.Transparent;
            }
            else if (block.Type == BlockType.Blue)
            {
                canvas.Background = new SolidColorBrush(Color.FromRgb(0x4d, 0x61, 0xf7));
            }
            else if (block.Type == BlockType.Gray)
            {
                canvas.Background = Brushes.Beige;
            }
        }
        public Canvas FindCanvasInGrid(System.Windows.Controls.Grid grid, int row, int column)
        {
            foreach (UIElement uie in grid.Children)
            {
                if (System.Windows.Controls.Grid.GetColumn(uie) == column &&
                    System.Windows.Controls.Grid.GetRow(uie) == row &&
                    uie is Canvas canvas)
                {
                    return canvas;
                }
            }

            return null;
        }
        public void ClearCell(int row, int column)
        {
            Canvas canvas = FindCanvasInGrid(_mainWindow.UIGrid, row, column);
            if (canvas != null)
            {
                canvas.Children.Clear();
            }
        }

        // Constants
        public const string CoinPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab5\\buttonsIco\\coin.png";
        public const string DoubleCoinPath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab5\\buttonsIco\\doubleCoin.png";
    }

    public class GameModel
    {
        public WindowTheme WindowTheme;
        public int CurrentLevelId;
        public List<Level> levelsList;

        public GameModel()
        {
            levelsList = new List<Level>();
        }
        public void AddLevel(Level newLevel)
        {
            if (levelsList != null)
            {
                levelsList.Add(newLevel);
            }
        }
        public void CreateLevel(Grid grid)
        {
            int newLevelId = (levelsList == null) ? 1 : levelsList.Count();
            Level newLevel = new Level(grid, newLevelId);
            this.AddLevel(newLevel);
        }
    }

    public class Level
    {
        private readonly int _id;
        private Grid _grid;
        private readonly int _requiredBlocksToLight;
        private int _lightedBlocksCount = 0;

        public Grid LevelGrid => _grid;

        public Level(Grid grid, int id)
        {
            if (grid == null)
            {
                throw new Exception("This grid has no blocks");
            }

            _grid = grid;
            _id = id;
            _requiredBlocksToLight = _grid.GetNumberOfBlocksToLight();
        }

        public void LightBlock()
        {
            ++_lightedBlocksCount;
            if (_lightedBlocksCount == _requiredBlocksToLight)
            {
                MessageBox.Show("You passed the level. Take your +10 coins.", "Congratulation!",
                    MessageBoxButton.OK, MessageBoxImage.Information);

                CoinsManager coinsManager = CoinsManager.GetInstance;
                coinsManager.AddCoins(10);
            }
        }

        public int GetId()
        {
            return _id;
        }

        public override string ToString()
        {
            return $"Level {_id}: {_lightedBlocksCount}/{_requiredBlocksToLight} blocks lit";
        }
    }
}