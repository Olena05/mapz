﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MAPZ_lab6.Patterns
{
    public class StrategyContext
    {
        private IStrategy _strategy;
        private MainWindow _mainWindow;

        public StrategyContext(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
        }

        public void SetStrategy(IStrategy strategy)
        {
            _strategy = strategy;
        }

        public void AddCommandToList(Command command)
        {
            _strategy.AddCommand(_mainWindow, command);
            _mainWindow.CommandsList.Add(command);
        }
    }

    public interface IStrategy
    {
        void AddCommand(MainWindow mainWindow, Command command);
    }

    public class EasyModeStrategy : IStrategy
    {
        public void AddCommand(MainWindow mainWindow, Command command)
        {
            var label = new Label();
            label.HorizontalAlignment = HorizontalAlignment.Center;
            label.Content = command.ToString();
            mainWindow.MainStackPanel.Children.Add(label);
        }
    }

    public class HardModeStrategy : IStrategy
    {
        public void AddCommand(MainWindow mainWindow, Command command)
        {
            var label = new Label();
            label.HorizontalAlignment = HorizontalAlignment.Center;
            label.Content = command.ToString();
            if (mainWindow.IsFooStackPanelSelected)
            {
                mainWindow.FooStackPanel.Children.Add(label);
            }
            else
            {
                mainWindow.MainStackPanel.Children.Add(label);
            }
        }
    }

    public enum Command
    {
        DoStep,
        TurnRight,
        TurnLeft,
        LightBlock,
        Foo
    }
}
