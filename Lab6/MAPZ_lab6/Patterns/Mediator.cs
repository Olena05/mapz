﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MAPZ_lab6.Patterns;

namespace MAPZ_lab6.Patterns
{
    public interface IMediator
    {
        void Notify(object sender, string ev);
    }

    public class MainWindowMediator : IMediator
    {
        private ThemeSwitcher _themeSwitcher;
        private ModeSwitcher _modeSwitcher;
        private MainWindow _mainWindow;
        private UIManager _uiManager;

        public MainWindowMediator(MainWindow mainWindow, UIManager uiManager, 
            ThemeSwitcher switchThemeBtn, ModeSwitcher switchModeBtn)
        {
            _themeSwitcher = switchThemeBtn;
            _themeSwitcher.SetMediator(this);
            _modeSwitcher = switchModeBtn;
            _modeSwitcher.SetMediator(this);
            _mainWindow = mainWindow;
            _uiManager = uiManager;
        }

        public void Notify(object sender, string ev)
        {
            if (sender == _themeSwitcher && ev == WindowTheme.Lab.ToString())
            {
                _themeSwitcher.MarkLabAsSelected();
                BitmapImage backgroundBitmap = new BitmapImage(new Uri("C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\backgrounds\\labBackground.jpg", UriKind.Relative));
                ImageBrush backgroundBrush = new ImageBrush(backgroundBitmap);
                _mainWindow.Background = backgroundBrush;

                string spritePath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\sprites\\robot.png";
                _uiManager.DrawSprite(0, 0, spritePath);
            }

            if (sender == _themeSwitcher && ev == WindowTheme.Space.ToString())
            {
                _themeSwitcher.MarkSpaceAsSelected();
                BitmapImage backgroundBitmap = new BitmapImage(new Uri("C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\backgrounds\\spaceBackground.jpg", UriKind.Relative));
                ImageBrush backgroundBrush = new ImageBrush(backgroundBitmap);
                _mainWindow.Background = backgroundBrush;

                string spritePath = "C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\sprites\\astronaut.png";
                _uiManager.DrawSprite(0, 0, spritePath);
            }

            if (sender == _modeSwitcher && ev == WindowMode.Easy.ToString())
            {
                _modeSwitcher.MarkEasyModeAsSelected();
                _mainWindow.FooBtn.Visibility = Visibility.Hidden;
                _mainWindow.FooStackPanel.Visibility = Visibility.Hidden;
                _mainWindow.MainStackPanel.Height = 450;
                _mainWindow.MainStackPanel.Background = new SolidColorBrush(Colors.Beige);

                var easyModeStrategy = new EasyModeStrategy();
                _mainWindow.Context.SetStrategy(easyModeStrategy);
            }

            if (sender == _modeSwitcher && ev == WindowMode.Hard.ToString())
            {
                _modeSwitcher.MarkHardModeAsSelected();
                _mainWindow.FooBtn.Visibility = Visibility.Visible;
                _mainWindow.FooStackPanel.Visibility = Visibility.Visible;
                _mainWindow.FooStackPanel.Height = 200;
                _mainWindow.MainStackPanel.Height = 200;

                var hardModeStrategy = new HardModeStrategy();
                _mainWindow.Context.SetStrategy(hardModeStrategy);
            }
        }
    }

    public class StartWindowMediator : IMediator
    {
        private ThemeSwitcher _themeSwitcher;
        private ModeSwitcher _modeSwitcher;
        private StartWindow _startWindow;

        public StartWindowMediator(StartWindow startWindow, 
            ThemeSwitcher switchThemeBtn, ModeSwitcher switchModeBtn)
        {
            _themeSwitcher = switchThemeBtn;
            _themeSwitcher.SetMediator(this);
            _modeSwitcher = switchModeBtn;
            _modeSwitcher.SetMediator(this);
            _startWindow = startWindow;
        }

        public void Notify(object sender, string ev)
        {
            if (sender == _themeSwitcher && ev == WindowTheme.Lab.ToString())
            {
                _themeSwitcher.MarkLabAsSelected();
                BitmapImage backgroundBitmap = new BitmapImage(new Uri("C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\backgrounds\\labBackground.jpg", UriKind.Relative));
                ImageBrush backgroundBrush = new ImageBrush(backgroundBitmap);
                _startWindow.Background = backgroundBrush;

                BitmapImage spriteBitmap = new BitmapImage(new Uri("sprites/robot.png", UriKind.Relative));
                _startWindow.SpritePicture.Source = spriteBitmap;

                _startWindow.ThemeForMainWindow = WindowTheme.Lab;
            }

            if (sender == _themeSwitcher && ev == WindowTheme.Space.ToString())
            {
                _themeSwitcher.MarkSpaceAsSelected();
                BitmapImage backgroundBitmap = new BitmapImage(new Uri("C:\\Users\\olena\\source\\repos\\MAPZ_lab6\\backgrounds\\spaceBackground.jpg", UriKind.Relative));
                ImageBrush backgroundBrush = new ImageBrush(backgroundBitmap);
                _startWindow.Background = backgroundBrush;

                BitmapImage spriteBitmap = new BitmapImage(new Uri("sprites/astronaut.png", UriKind.Relative));
                _startWindow.SpritePicture.Source = spriteBitmap;

                _startWindow.ThemeForMainWindow = WindowTheme.Space;
            }

            if (sender == _modeSwitcher && ev == WindowMode.Easy.ToString())
            {
                _modeSwitcher.MarkEasyModeAsSelected();
                _startWindow.ModeForMainWindow = WindowMode.Easy;
            }

            if (sender == _modeSwitcher && ev == WindowMode.Hard.ToString())
            {
                _modeSwitcher.MarkHardModeAsSelected();
                _startWindow.ModeForMainWindow = WindowMode.Hard;
            }
        }
    }

    public class BaseComponent
    {
        protected IMediator _mediator;

        public BaseComponent(IMediator mediator = null)
        {
            _mediator = mediator;
        }

        public void SetMediator(IMediator mediator)
        {
            _mediator = mediator;
        }
    }

    public class ThemeSwitcher : BaseComponent
    {
        private Button _labThemeButton;
        private Button _spaceThemeButton;

        public void SetButtons(Button labButton, Button spaceButton)
        {
            _labThemeButton = labButton;
            _labThemeButton.Click += (sender, args) => SetLabTheme();
            _spaceThemeButton = spaceButton;
            _spaceThemeButton.Click += (sender, args) => SetSpaceTheme();
        }

        public void MarkLabAsSelected()
        {
            _labThemeButton.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
            _labThemeButton.Foreground = new SolidColorBrush(Colors.AliceBlue);
            _spaceThemeButton.Background = new SolidColorBrush(Colors.Transparent);
            _spaceThemeButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
        }

        public void MarkSpaceAsSelected()
        {
            _spaceThemeButton.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
            _spaceThemeButton.Foreground = new SolidColorBrush(Colors.AliceBlue);
            _labThemeButton.Background = new SolidColorBrush(Colors.Transparent);
            _labThemeButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
        }

        public void SetLabTheme()
        {
            _mediator.Notify(this, WindowTheme.Lab.ToString());
        }

        public void SetSpaceTheme()
        {
            _mediator.Notify(this, WindowTheme.Space.ToString());
        }
    }

    public class ModeSwitcher : BaseComponent
    {
        private Button _easyModeButton;
        private Button _hardModeButton;

        public void SetButtons(Button labButton, Button spaceButton)
        {
            _easyModeButton = labButton;
            _easyModeButton.Click += (sender, args) => SetEasyMode();
            _hardModeButton = spaceButton;
            _hardModeButton.Click += (sender, args) => SetHardMode();
        }

        public void MarkEasyModeAsSelected()
        {
            _easyModeButton.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
            _easyModeButton.Foreground = new SolidColorBrush(Colors.AliceBlue);
            _hardModeButton.Background = new SolidColorBrush(Colors.Transparent);
            _hardModeButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
        }

        public void MarkHardModeAsSelected()
        {
            _hardModeButton.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
            _hardModeButton.Foreground = new SolidColorBrush(Colors.AliceBlue);
            _easyModeButton.Background = new SolidColorBrush(Colors.Transparent);
            _easyModeButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4d61f7"));
        }

        public void SetEasyMode()
        {
            _mediator.Notify(this, WindowMode.Easy.ToString());
        }

        public void SetHardMode()
        {
            _mediator.Notify(this, WindowMode.Hard.ToString());
        }
    }

    public enum WindowMode
    {
        Easy,
        Hard,
    }

    public enum WindowTheme
    {
        Lab,
        Space,
    }
}
