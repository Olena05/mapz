﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab6.Patterns
{
    public interface ICommand
    {
        void Execute();
    }

    class ShowAccountInfoCommand : ICommand
    {
        public void Execute()
        {
            var accountWindow = new AccountWindow();
            accountWindow.Show();
        }
    }

    class ShowGameInfoCommand : ICommand
    {
        public void Execute()
        {
            var infoWindow = new InfoWindow();
            infoWindow.Show();
        }
    }
}
