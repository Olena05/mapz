﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab6.Patterns
{
    public sealed class CoinsManager
    {

        private static CoinsManager _instance;
        private static int _coinsCount;

        private CoinsManager()
        {
            _coinsCount = 0;
        }

        public static CoinsManager GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CoinsManager();
                }

                return _instance;
            }
        }

        public void AddCoins(int amount)
        {
            _coinsCount += amount;
        }

        public void RemoveCoins(int amount)
        {
            _coinsCount -= amount;
        }

        public int GetCoinsNumber
        {
            get { return _coinsCount; }
        }
    }
}
