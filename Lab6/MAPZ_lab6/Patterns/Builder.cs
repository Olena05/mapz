﻿using System;
using System.CodeDom;
using System.Text;
using System.Collections.Generic;
using System.Windows;
using Block = MAPZ_lab6.Patterns.Grid.Block;

namespace MAPZ_lab6.Patterns
{
    public interface IGridBuilder
    {
        void SetSize(int width, int height);
        void SetBlock(int x, int y, BlockType type);
        void SetBlocks(List<(int x, int y, BlockType type)> blockPositions);
        void SetCoinOnBlock(int x, int y);
        void SetDoubleCoinOnBlock(int x, int y);
    }

    public class GridBuilder : IGridBuilder
    {
        private Grid _grid = new Grid();
        public GridBuilder()
        {
            this.Reset();
        }
        public void Reset()
        {
            this._grid = new Grid();
        }
        public void SetSize(int width, int height)
        {
            if (_grid.Width == Grid.DefaultWidth &&
                _grid.Height == Grid.DefaultHeight &&
                width < 8 && width > 2 &&
                height < 6 && height > 2)
            {
                _grid = new Grid(width, height);
            }
        }
        public void SetBlock(int x, int y, BlockType type)
        {
            if (_grid.GetBlock(x, y)?.Type != type)
            {
                _grid.SetBlockToPosition(x, y, type);
            }
        }
        public void SetBlocks(List<(int x, int y, BlockType type)> blockPositions)
        {
            foreach (var (x, y, type) in blockPositions)
            {
                SetBlock(x, y, type);
            }
        }
        public void SetCoinOnBlock(int x, int y)
        {
            Block block = _grid.GetBlock(x, y);
            block?.SetCoins(1);
        }
        public void SetDoubleCoinOnBlock(int x, int y)
        {
            Block block = _grid.GetBlock(x, y);
            block?.SetCoins(2);
        }
        public Grid BuildGrid()
        {
            var result = this._grid;
            Reset();
            return result;
        }
    }

    public class GridDirector
    {
        private IGridBuilder _builder;
        public IGridBuilder Builder
        {
            set { _builder = value; }
        }
        public void BuildEasyGridForLevel1()
        {
            _builder.SetSize(3, 4);

            _builder.SetBlocks(new List<(int x, int y, BlockType type)>()
            {
                (0, 0, BlockType.Gray),
                (0, 1, BlockType.Gray),
                (0, 4, BlockType.Blue),
                (1, 0, BlockType.Blue),
                (1, 1, BlockType.Gray),
                (1, 4, BlockType.Gray),
                (2, 1, BlockType.Blue),
                (2, 2, BlockType.Gray)
            });

            _builder.SetCoinOnBlock(1, 1);
            _builder.SetCoinOnBlock(2, 2);
            _builder.SetCoinOnBlock(3, 2);
            _builder.SetCoinOnBlock(4, 0);
        }
        public void BuildEasyGridForLevel2()
        {
            _builder.SetSize(4, 4);

            _builder.SetBlocks(new List<(int x, int y, BlockType type)>()
            {
                (0, 0, BlockType.Gray),
                (0, 1, BlockType.Gray),
                (0, 3, BlockType.Blue),
                (1, 0, BlockType.Gray),
                (1, 1, BlockType.Gray),
                (1, 3, BlockType.Gray),
                (2, 1, BlockType.Blue),
                (2, 2, BlockType.Gray),
                (2, 3, BlockType.Gray),
                (3, 0, BlockType.Blue),
                (3, 1, BlockType.Blue),
                (3, 2, BlockType.Gray)
            });

            _builder.SetCoinOnBlock(1, 0);
            _builder.SetCoinOnBlock(2, 2);
            _builder.SetCoinOnBlock(3, 2);

            _builder.SetDoubleCoinOnBlock(1, 1);
        }
    }

    public class Grid
    {
        private Block[,] _blocks;
        private int _levelId;
        public const int DefaultWidth = 1;
        public const int DefaultHeight = 1;

        public int Width => _blocks.GetLength(0);
        public int Height => _blocks.GetLength(1);

        public Grid(int width = 1, int height = 1)
        {
            _blocks = new Block[width, height];
        }
        public int GetNumberOfBlocksToLight()
        {
            int count = 0;
            foreach (var block in _blocks)
            {
                if (block != null && block.Type == BlockType.Blue)
                {
                    ++count;
                }
            }
            return count;
        }
        public void SetBlockToPosition(int x, int y, BlockType type)
        {
            if (x >= 0 && y >= 0 && x < Width && y < Height)
            {
                _blocks[x, y] = new Block(type);
            }
        }
        public Block GetBlock(int x, int y)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
            {
                return null;
            }
            return _blocks[x, y];
        }
        public Block[,] GetAllBlocks()
        {
            return _blocks;
        }
        public class Block
        {
            private BlockType _type;
            private int _coinsCount;

            public BlockType Type => _type;
            public int CoinsCount => _coinsCount;

            public Block(BlockType type)
            {
                _type = type;
                _coinsCount = 0;
            }
            public void SetCoins(int coinsNumber)
            {
                _coinsCount = coinsNumber;
            }
        }
    }

    public enum BlockType
    {
        Gray, // default block
        Blue // block to light
    }
}