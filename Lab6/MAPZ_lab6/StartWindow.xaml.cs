﻿using System.Windows;
using System.Windows.Controls;
using MAPZ_lab6.Patterns;

namespace MAPZ_lab6
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        private GameModel _model;
        private CoinsManager _coinsManager;
        private MainWindowMediator _mediator;
        private UIManager uiManager;
        public WindowTheme ThemeForMainWindow;
        public WindowMode ModeForMainWindow;

        public StartWindow()
        {
            InitializeComponent();
            _model = new GameModel();
            ThemeForMainWindow = WindowTheme.Lab;
            ModeForMainWindow = WindowMode.Easy;

            var themeSwitcher = new ThemeSwitcher();
            themeSwitcher.SetButtons(LabThemeBtn, SpaceThemeBtn);

            var modeSwitcher = new ModeSwitcher();
            modeSwitcher.SetButtons(EasyModeBtn, HardModeBtn);

            var mediator = new StartWindowMediator(this, themeSwitcher, modeSwitcher);
        }

        private void StartGameBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow(ThemeForMainWindow, ModeForMainWindow);

            mainWindow.Show();
            this.Close();
        }

        private void CommandBtn_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            (button.Tag as ICommand).Execute();
        }
    }
}
