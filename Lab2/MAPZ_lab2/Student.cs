﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace MAPZ_lab2
{
    public class Student : Person, IStudent
    {
        public readonly string UniversityName;
        public static int StudentsCount = 0;

        public Student(string firstName, string lastName, string universityName = "Lviv Polytechnic")
            : base(firstName, lastName)
        {
            UniversityName = universityName;
            Console.WriteLine("Dynamic constructor");
        }

        static Student()
        {
            ++StudentsCount;
            Console.WriteLine("Static constructor");
        }

        public override void Sleep()
        {
            Console.WriteLine($"Student {FirstName} is sleeping");
        }

        public void Study()
        {
            Console.WriteLine("Student is studying");
        }

        public static implicit operator string(Student student) => $"{student.FirstName} {student.LastName}";

        public static explicit operator int(Student student) => student.FirstName.Length;

        public override string ToString()
        {
            return $"{FirstName} {LastName} is a student at {UniversityName}.";
        }

        public override bool Equals(object obj)
        {
            if (obj is Student otherStudent)
            {
                return FirstName == otherStudent.FirstName && LastName == otherStudent.LastName && UniversityName == otherStudent.UniversityName;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return FirstName.Length.GetHashCode() + LastName.Length.GetHashCode();
        }
    }
}
