﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    public abstract class Person
    {
        public readonly string FirstName;
        public readonly string LastName;

        public string HigherEducationType { get; }

        protected Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        protected Person(string firstName, string lastName, string higherEducationType)
            : this(firstName, lastName)
        {
            HigherEducationType = higherEducationType;
        }

        public abstract void Sleep();
    }
}
