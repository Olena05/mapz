﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    internal interface IStudent
    {
        void Study();
    }
}
