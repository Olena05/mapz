﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    public class Worker : Person
    {
        public readonly string CompanyName;

        public Worker(string FirstName, string LastName, string CompanyName)
            : base(FirstName, LastName)
        {
            this.CompanyName = CompanyName;
        }

        public Worker(string FirstName, string LastName, string HigherEducationType, string CompanyName)
            : base(FirstName, LastName, HigherEducationType)
        {
            this.CompanyName = CompanyName;
        }

        private WorkersCar car;

        public void Work(Person person)
        {
            Console.WriteLine($"Worker {person.FirstName} is working in {CompanyName}");
        }

        public override void Sleep()
        {
            Console.WriteLine($"Worker {FirstName} is sleeping");
        }

        public class WorkersCar : Car
        {
            public void GoToWork()
            {
                Console.WriteLine($"Worker is driving to the office");
            }

            public WorkersCar(string model, string brand, Colors color)
                : base(model, brand, color) { }
        }
    }
}
