﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Testing '&' operation for Colors enum
            Car car = new Car("Mercedes-Benz", "CLS", Colors.Black | Colors.White);

            //Testing constructors calling sequance
            Student student1 = new Student("Marry", "Abrams", "Kyiv Polytechnic");
            Student student2 = new Student("David", "Smith", "Kyiv Polytechnic");
            Student student3 = new Student("Liza", "Smith", "Kyiv Polytechnic");
            Student student4 = new Student("Paul", "Atreides", "Lviv Polytechnic");

            //Testing boxing and unboxing
            int value = 42;
            object boxValue = value;
            int unboxedValue = (int)boxValue;

            TestObjectMethods();
        }

        static void TestObjectMethods()
        {
            var student = new Student("John", "Wick", "Example University");

            Console.WriteLine($"Student Information: {student}");
            Console.WriteLine($"Is student equal to itself? {student.Equals(student)}");
            Console.WriteLine($"Student's hashcode: {student.GetHashCode()}");
        }

        static void TestImplicitAndExplicit()
        {
            Student student = new Student("John", "Wick", "Example University");

            string studentName = student;
            int firstNameLength = (int)student;

            Console.WriteLine($"Implicit casting: {studentName} {firstNameLength}");

            int length = (int)student;

            Console.WriteLine($"Explicit casting: {length}");
        }

        static void TestRefAndOutFunctions()
        {
            var value1 = 5;
            var value2 = 5;
            var value3 = 5;

            Console.WriteLine(value1 + " " + value2 + " " + value3);

            IncByOneWithRef(ref value1);
            GetRandomNumber(out value2);
            IncByOneWithoutRef(value3);
            GetRandomNumber(out int value4);

            Console.WriteLine(value1 + " " + value2 + " " + value3 + " " + value4);

            PrintMessage("Message to be printed");
        }

        static void PrintMessage(string message)
        {
            Console.WriteLine("Message to be printed");
        }

        static void IncByOneWithRef(ref int value)
        {
            value += 1;
        }

        static void IncByOneWithoutRef(int value)
        {
            value += 1;
        }

        static void GetRandomNumber(out int number)
        {
            Random random = new Random();
            number = random.Next(0, 100);
        }

    }

    struct StudentStruct
    {
        string FirstName;
        string LastName;

        void Sleep() { }
    }

    [Flags]
    public enum Colors
    {
        None = 0,
        Red = 1 << 0,       
        Green = 1 << 1,     
        Blue = 1 << 2,
        LightGray = White >> 1,
        White = Red | Green | Blue, 
        Black = ~White,
        Cyan = Green & Blue
    }
}
