﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    public class Car
    {
        public readonly string Model;
        public readonly string Brand;
        public readonly Colors Color;

        public Car(string model, string brand, Colors color)
        {
            Model = model;
            Brand = brand;
            Color = color;
        }

        public Colors GetColor()
        {
            return this.Color;
        }

        public void Go()
        {
            Console.WriteLine("Car is going");
        }

        public void Go(string destination)
        {
            Console.WriteLine($"Car is going to {destination}");
        }
    }
}
