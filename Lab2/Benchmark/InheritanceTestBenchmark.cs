﻿using System.Reflection;
using BenchmarkDotNet;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using InheritanceTest;

BenchmarkSwitcher
    .FromAssembly(Assembly.GetExecutingAssembly())
    .Run(args);

namespace Benchmark
{
    [Config(typeof(AntiVirusFriendlyConfig))]
    [MemoryDiagnoser]
    [RankColumn]
    public class InheritanceTestWithUsingBenchmark
    {
        [Benchmark]
        public void TestBaseClassInInheritChain()
        {
            using (Person peron = new Person("FirstName", "LsstName", 17))
            {

            }
        }

        [Benchmark]
        public void TestFirstClassInInheritChain()
        {
            using (Teenager teen = new Teenager("FirstName", "LsstName", 17))
            {

            }
        }

        [Benchmark]
        public void TestSecondClassInInheritChain()
        {
            using (Student student = new Student("FirstName", "LsstName", 17))
            {

            }
        }

        [Benchmark]
        public void TestThirdClassInInheritChain()
        {
            using (Worker worker = new Worker("FirstName", "LsstName", 17))
            {

            }
        }

        [Benchmark]
        public void TestFourthClassInInheritChain()
        {
            using (Driver driver = new Driver("FirstName", "LsstName", 17))
            {

            }
        }
    }
}