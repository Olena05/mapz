﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
          
        }
    }

    public class Person : IDisposable
    {
        void IDisposable.Dispose()
        {

        }

        private string FirstName;
        private string LastName;
        private int Age;

        public Person(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }
    }

    public class Teenager : Person
    {
        public Teenager(string firstName, string lastName, int age)
            :base(firstName, lastName, age)
        { }
    }

    public class Student : Teenager
    {
        public Student(string firstName, string lastName, int age)
            : base(firstName, lastName, age)
        { }
    }

    public class Worker : Student
    {
        public Worker(string firstName, string lastName, int age)
            : base(firstName, lastName, age)
        { }
    }

    public class Driver : Worker
    {
        public Driver(string firstName, string lastName, int age)
            : base(firstName, lastName, age)
        { }
    }
}
